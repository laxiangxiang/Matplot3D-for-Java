import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;

public class Curver2dsDemo {

	public static void main(String[] args) {

		MatPlot3DMgr mgr = new MatPlot3DMgr();

		mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_CURVE2DS);
		Random ra = new Random();

		int groupCount = 5;

		int pointCount = 25;

		List<Point2D.Double>[] targets = new ArrayList[pointCount];

		// 设置最终的数据
		for (int c = 1; c <= targets.length; c++) {

			List<Point2D.Double> li = new ArrayList<Point2D.Double>();
			targets[c - 1] = li;

			for (int i = 0; i < pointCount; i++) {
				li.add(new Point2D.Double(i, i * c * 0.05 + (ra.nextDouble() * 0.8)));
			}
		}

		mgr.addData2D("Item系列 1", null, targets[0]);
		mgr.addData2D("Item系列 2", null, targets[1]);
		mgr.addData2D("Item系列 3", null, targets[2]);
		mgr.addData2D("Item系列 4", null, targets[3]);
		mgr.addData2D("Item系列 5", null, targets[4]);

		mgr.setBeita(4.3);
		mgr.setSeeta(0.3);

		mgr.setScaleY(1); // 压缩Y方向,使图形显得更薄
		mgr.setScaleX(1);
		mgr.setScaleZ(1.6);

		mgr.setTitle("Demo : 多层2维趋势图表");
		
		mgr.getProcessor().setRulerYVisable(false);
		
		mgr.getProcessor().setXName("X_月份");
		mgr.getProcessor().setZName("Z_指标");

		mgr.getProcessor().setRulerTextColor(new Color(11, 79, 107));
		mgr.getProcessor().setRulerTextFont(new Font("微软雅黑", Font.BOLD, 13));
		
		mgr.getProcessor().setAxisNameTextColor(new Color(138, 171, 205));

		// 自定义标尺
		String[] labels = new String[13];
		double[] positions = new double[13];

		labels[0] = "    2019    "; // 加入空格会扩大文字和标尺的距离
		positions[0] = 0;

		for (int i = 2; i < pointCount; i += 2) {
			labels[i / 2] = i / 2 + "月";
			positions[i / 2] = i;
		}

		mgr.getProcessor().setRulerLabelsX(labels, positions);

		mgr.getProcessor().setRulerYVisable(false);

		mgr.getProcessor().setCoordinateSysShowType(mgr.getProcessor().COORDINATE_SYS_STABLE);

		mgr.show();
	}
}
